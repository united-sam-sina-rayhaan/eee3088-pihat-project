
# EEE3088 PiHat Project

# Description
Our device collects audio and compares it to frequency data for male and female voices.
The Gender device! 

Now a deaf man can tell the gender of any person. It lights up, letting
him know if someone is talking to him.

If someone needs to see someone else's gender based on their voice, this device can do just that. This is shown by a
colour relating to the gender!

Flashes green for males, and red for females. Blue for babies and yellow for unknown
frequencies.

------------------------------------------------------------------------------------------

# How to contribute

1. Just be reasonable
2. Communicate with the team
3. Actually do work
4. Use git correctly
